import { google } from 'googleapis';
import type { NextApiRequest, NextApiResponse } from 'next'

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const oauth2Client = new google.auth.OAuth2(
    '864459711795-1skvstjh7tuil7v54o8lcq3qpshi8pd7.apps.googleusercontent.com',
    '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',
    'http://localhost:3000/api/oauth2callback'
  );

  // generate a url that asks permissions for Blogger and Google Calendar scopes
  const scopes = ['https://www.googleapis.com/auth/calendar'];

  const url = oauth2Client.generateAuthUrl({
    // If you only need one scope you can pass it as a string
    scope: scopes,
  });
  res.redirect(url);
}
