import { google } from 'googleapis';
import type { NextApiRequest, NextApiResponse } from 'next'

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const oauth2Client = new google.auth.OAuth2(
    '864459711795-1skvstjh7tuil7v54o8lcq3qpshi8pd7.apps.googleusercontent.com',
    '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',
    'http://localhost:3000/api/oauth2callback'
  );
  const { code } = req.query
  const { tokens } = await oauth2Client.getToken(code as string)
  oauth2Client.setCredentials(tokens);

  const event = {
    'summary': 'Google I/O 2015',
    'location': '800 Howard St., San Francisco, CA 94103',
    'description': 'A chance to hear more about Google\'s developer products.',
    'start': {
      'dateTime': '2015-05-28T09:00:00-07:00',
      'timeZone': 'America/Los_Angeles',
    },
    'end': {
      'dateTime': '2015-05-28T17:00:00-07:00',
      'timeZone': 'America/Los_Angeles',
    },
    'recurrence': [
      'RRULE:FREQ=DAILY;COUNT=2'
    ],
    'attendees': [
      // { 'email': 'lpage@example.com' },
      // { 'email': 'sbrin@example.com' },
    ],
    'reminders': {
      'useDefault': false,
      'overrides': [
        { 'method': 'email', 'minutes': 24 * 60 },
        { 'method': 'popup', 'minutes': 10 },
      ],
    },
    conferenceData: {
      createRequest: { requestId: "czasdasd" },
    },
  };

  const calendar = google.calendar({ version: 'v3', auth: oauth2Client });
  const resp = await calendar.events.insert({
    requestBody: event,
    auth: oauth2Client,
    calendarId: 'primary',
    conferenceDataVersion: 1,
  })

  res.status(resp.status).json(resp.data?.conferenceData?.entryPoints?.[0]?.uri)
}
